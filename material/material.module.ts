import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatOptionModule,
  MatCheckboxModule,
  MatButtonToggleModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatCardModule,
  MatRadioModule
} from '@angular/material';

const modules = [
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatOptionModule,
  MatCheckboxModule,
  MatRadioModule,
  MatButtonToggleModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatCardModule
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ...modules
  ],
  exports: [
    ...modules
  ]
})
export class ZeusMaterialModule { }
