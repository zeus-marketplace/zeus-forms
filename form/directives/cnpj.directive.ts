import { Directive, HostListener, ElementRef, Input } from '@angular/core';
import { NgControl, FormControl, AbstractControl } from '@angular/forms';
import { ZeusDocumentValidator } from '../validators/document.validator';

@Directive({
    selector: '[zeusCNPJ]',
})
export class ZeusCNPJDirective {

    @Input('formControl') formControl: AbstractControl;
    constructor(private input: ElementRef) { }

    ngOnInit(): void {
        this.formControl.setValidators(ZeusDocumentValidator.validate);
    }

    @HostListener('input', ['$event'])
    onInput(event) {

        let value = event.target.value.replace(/[^0-9]+/g, '').slice(0, 14);
        value = value.replace(/^(\d{2})(\d)/, '$1.$2'); //Coloca ponto entre o segundo e o terceiro dígitos
        value = value.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3'); //Coloca ponto entre o quinto e o sexto dígitos
        value = value.replace(/\.(\d{3})(\d)/, '.$1/$2'); //Coloca uma barra entre o oitavo e o nono dígitos
        value = value.replace(/(\d{4})(\d)/, '$1-$2'); //Coloca um hífen depois do bloco de quatro dígitos

        this.formControl.setValue(value);

    }

}