import { Directive, HostListener, ElementRef, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { FieldComponent } from '../field/field.component';

@Directive({
    selector: '[zeusCEP]',
})
export class ZeusCEPDirective {

    @Input('formControl') formControl: AbstractControl;
    constructor(private input: ElementRef, private host: FieldComponent) { }

    ngOnInit(): void {
        console.log(this.host);
    }

    @HostListener('input', ['$event'])
    onInput(event) {

        let value = event.target.value.replace(/[^0-9]+/g, '').slice(0, 8);
        this.host.complete.emit({
            cep: value
        });

        value = value.replace(/^(\d{2})(\d)/, '$1.$2'); //Coloca ponto entre o segundo e o terceiro dígitos
        value = value.replace(/(\d)(\d{3})$/, '$1-$2'); //Coloca ponto entre o quinto e o sexto dígitos

        this.formControl.setValue(value);

    }

}