import { Directive, HostListener, ElementRef, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { FieldComponent } from '../field/field.component';
import { ZeusPhoneValidator } from '../validators/phone.validator';

@Directive({
    selector: '[zeusTelefone]',
})
export class ZeusTelefoneDirective {

    @Input('formControl') formControl: AbstractControl;
    constructor(private input: ElementRef, private host: FieldComponent) { }

    ngOnInit(): void {
        this.formControl.setValidators(ZeusPhoneValidator.validate);
    }

    @HostListener('input', ['$event'])
    onInput(event) {

        let value = event.target.value.replace(/[^0-9]+/g, '').slice(0, 11);
        this.host.complete.emit({
            telefone: value
        });

        if (value.length <= 8) {
            value = value.replace(/^(\d{4})(\d)/, '$1-$2');
        } else if (value.length <= 9) {
            value = value.replace(/^(\d)(\d)/, '$1 $2');
            value = value.replace(/(\d)(\d{4})$/, '$1-$2');
        } else if (value.length <= 10) {
            value = value.replace(/^(\d{2})(\d)/, '($1) $2');
            value = value.replace(/(\d)(\d{4})$/, '$1-$2');
        } else {
            value = value.replace(/^(\d{2})(\d)/, '($1) $2');
            value = value.replace(/(\d)(\d{8})$/, '$1 $2');
            value = value.replace(/(\d)(\d{4})$/, '$1-$2');
        }

        this.formControl.setValue(value);

    }

}