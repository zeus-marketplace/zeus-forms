import { Directive, HostListener, ElementRef, Input } from '@angular/core';
import { NgControl, FormControl, AbstractControl } from '@angular/forms';
import { ZeusDocumentValidator } from '../validators/document.validator';

@Directive({
    selector: '[zeusCPF]',
})
export class ZeusCPFDirective {

    @Input('formControl') formControl: AbstractControl;
    constructor(private input: ElementRef){}
    
    ngOnInit(): void {
        this.formControl.setValidators(ZeusDocumentValidator.validate);
    }

    @HostListener('input', ['$event'])
    onInput(event){
        
        let value = event.target.value.replace(/[^0-9]+/g, '').slice(0, 11);
        value = value.replace(/([0-9]{3})([0-9])/, '$1.$2');
        value = value.replace(/\.([0-9]{3})([0-9])/, '.$1.$2');
        value = value.replace(/([0-9])([0-9]{1,2})$/, '$1-$2');

        this.formControl.setValue(value);
        
    }
    
}