import { Validator, AbstractControl, ValidationErrors } from '@angular/forms';

export class ZeusPhoneValidator implements Validator {

    static phoneLength = 9;
    static celphoneLength = 11;
    static phoneDDDLength = 14;
    static celphoneDDDLength = 16;

    /**
     * Valida um CPF ou CNPJ de acordo com seu dígito verificador.
     */
    static validate(c: AbstractControl): ValidationErrors | null {

        const phone = c.value;

        // Verifica o tamanho da string.
        const lengthArray = [
            ZeusPhoneValidator.phoneLength, 
            ZeusPhoneValidator.celphoneLength, 
            ZeusPhoneValidator.phoneDDDLength, 
            ZeusPhoneValidator.celphoneDDDLength
        ];
        if (lengthArray.indexOf(phone.length) < 0) {
            return { length: true };
        }

        const regexArray: RegExp[] = [
            /^\d{4}-\d{4}$/,
            /^\d\s\d{4}-\d{4}$/,
            /^\(\d{2}\)\s\d{4}-\d{4}$/,
            /^\(\d{2}\)\s\d\s\d{4}-\d{4}$/
        ];
        const valid = regexArray.some(regex => regex.test(phone));

        if (!valid) {
            // Dígito verificador não é válido, resultando em falha.
            return { digit: true };
        }

        return null;
    }

    /**
     * Implementa a interface de um validator.
     */
    validate(c: AbstractControl): ValidationErrors | null {
        return ZeusPhoneValidator.validate(c);
    }
}