import { Directive } from '@angular/core';
import { Validator, NG_VALIDATORS, AbstractControl, ValidationErrors } from '@angular/forms';

class ZeusDocumentValidator implements Validator {

    static cpfLength = 11;
    static cnpjLength = 14;

    /**
     * Calcula o dígito verificador do CPF ou CNPJ.
     */
    static buildDigit(arr: number[]): number {

        const isCpf = arr.length < ZeusDocumentValidator.cpfLength;
        const digit = arr
                .map((val, idx) => val * ((!isCpf ? idx % 8 : idx) + 2))
                .reduce((total, current) => total + current) % ZeusDocumentValidator.cpfLength;

        if (digit < 2 && isCpf) {
            return 0;
        }

        return ZeusDocumentValidator.cpfLength - digit;
    }

    /**
     * Valida um CPF ou CNPJ de acordo com seu dígito verificador.
     */
    static validate(c: AbstractControl): ValidationErrors | null {

        const document = c.value.replace(/\D/g, '');

        // Verifica o tamanho da string.
        if ([ZeusDocumentValidator.cpfLength, ZeusDocumentValidator.cnpjLength].indexOf(document.length) < 0) {
            return { length: true };
        }

        // Verifica se todos os dígitos são iguais.
        if (/^([0-9])\1*$/.test(document)) {
            return { equalDigits: true };
        }

        // A seguir é realizado o cálculo verificador.
        const documentArr: number[] = document.split('').reverse().slice(2);

        documentArr.unshift(ZeusDocumentValidator.buildDigit(documentArr));
        documentArr.unshift(ZeusDocumentValidator.buildDigit(documentArr));

        if (document !== documentArr.reverse().join('')) {
            // Dígito verificador não é válido, resultando em falha.
            return { digit: true };
        }

        return null;
    }

    /**
     * Implementa a interface de um validator.
     */
    validate(c: AbstractControl): ValidationErrors | null {
        return ZeusDocumentValidator.validate(c);
    }
}

@Directive({
    selector: '[zeusDocument]',
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: ZeusDocumentDirective,
        multi: true
    }]
})
class ZeusDocumentDirective extends ZeusDocumentValidator implements Validator { }

export {
    ZeusDocumentDirective,
    ZeusDocumentValidator
}