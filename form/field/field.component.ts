import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { FormControl, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FieldComponent {

  @Input() type;
  @Input() placeholder;
  @Input() maxLength = null;
  @Input() options;
  @Input() label;
  @Input() rows = 8;
  @Input() value: 'value';
  @Input() multiple = false;
  @Input() control: FormControl = this.fb.control(
    this.fb.control
      ? { item: [this.multiple ? [] : null] }
      : {}
  );

  @Output() complete = new EventEmitter();

  constructor(private fb: FormBuilder) { }

}
