// Angular Basic
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Angular Forms
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// Angular Material
import { ZeusMaterialModule } from '../material/material.module';

// Components
import { FieldComponent } from './field/field.component';
import { ZeusCPFDirective } from './directives/cpf.directive';
import { ZeusDocumentDirective } from './validators/document.validator';
import { ZeusCNPJDirective } from './directives/cnpj.directive';
import { ZeusCEPDirective } from './directives/cep.directive';
import { ZeusTelefoneDirective } from './directives/telefone.directive';

@NgModule({
  declarations: [
    FieldComponent,
    ZeusCPFDirective,
    ZeusCNPJDirective,
    ZeusCEPDirective,
    ZeusDocumentDirective,
    ZeusTelefoneDirective
  ],
  imports: [
    ZeusMaterialModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [ZeusMaterialModule, FieldComponent]
})
export class ZeusFormModule { }
